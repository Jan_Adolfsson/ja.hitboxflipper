﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Ja.HitBoxFlip
{
	public class Polygon
	{
		public List<Point> Vertexes { get; set; }
		public string Purpose { get; set; }
		public List<string> Tags { get; set; }
	}
}
