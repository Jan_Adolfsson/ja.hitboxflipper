﻿using System;
using System.Drawing;
using CommandLineParser.Arguments;

namespace Ja.HitBoxFlip
{
	class ParserTarget
	{
		[ValueArgument(
			typeof(string), 
			'i', 
			"input", 
			Description = "Input path to json-file with hitbox coordinates", 
			Optional = false
		)]
		public string Input;

		[ValueArgument(
			typeof(string), 
			'o', 
			"o", 
			Description = "Output path to json-file with flipped hitbox coordinates",
			Optional = false
		)]
		public string Output;

		[ValueArgument(
			typeof(string), 
			's', 
			"size", 
			Description = "Tile size e.g. 64,64",
			Optional = false
		)]
		public string Size;

		[EnumeratedValueArgument(
			typeof(string), 
			'd', 
			"direction", 
			AllowedValues = "horizontal;vertical", 
			Description = "Flip direction",
			Optional = false
		)]
		public string Direction;
	}
}
