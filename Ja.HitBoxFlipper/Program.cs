﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using CommandLineParser.Exceptions;
using Newtonsoft.Json;

namespace Ja.HitBoxFlip
{
	class Program
	{
		static void Main(string[] args)
		{
			ParserTarget parserTarget = new ParserTarget();
			if (!ParseCommandLineInput(parserTarget, args))
				return;

			if (!ParseSize(parserTarget.Size, out Size tileSize))
				return;


			if (!ReadInput(parserTarget.Input, out List<Tile> tiles))
				return;

			if (!Flip(parserTarget.Direction, tileSize, tiles, out List<Tile> tilesFlipped))
				return;

			WriteOutput(parserTarget.Output, tilesFlipped);
		}

		private static bool ParseSize(string size, out Size tileSize)
		{
			tileSize = Size.Empty;
			try
			{
				tileSize = ParseSize(size);
				return true;
			}
			catch
			{
				Console.WriteLine("Invalid format of size.");
				return false;
			}
		}
		private static Size ParseSize(string size)
		{
			var trimmed = size.Trim();
			var parts = trimmed.Split(',');
			var width = int.Parse(parts[0]);
			var height = int.Parse(parts[1]);
			return new Size(width, height);
		}
		private static bool ParseCommandLineInput(ParserTarget parserTarget, string[] args)
		{
			CommandLineParser.CommandLineParser parser =
				new CommandLineParser.CommandLineParser();
			parser.ExtractArgumentAttributes(parserTarget);

			try
			{
				parser.ParseCommandLine(args);
				return true;
			}
			catch (UnknownArgumentException uae)
			{
				Console.WriteLine($"Unknown argument {uae.Argument}");
			}
			catch (CommandLineFormatException clfe)
			{
				Console.WriteLine($"Invalid argument format {clfe.Message}");
			}
			catch (CommandLineArgumentOutOfRangeException claoore)
			{
				Console.WriteLine($"Invalid value {claoore.Message}");
			}
			catch (MandatoryArgumentNotSetException manse)
			{
				Console.WriteLine($"Mandatory argument not set {manse.Argument}");
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Unhandled exception.\r\n{ex}");
			}

			return false;
		}

		private static bool Flip(string direction, Size tileSize, List<Tile> tiles, out List<Tile> tilesFlipped)
		{
			tilesFlipped = null;
			if (direction == "horizontal")
			{
				try
				{
					tilesFlipped = CreateHorizontallyFlippedTiles(tileSize, tiles);
					return true;
				}
				catch
				{
					Console.WriteLine("Failed flipping tiles.");
				}
			}
			else
			{
				Console.WriteLine("Only direction horizontal supported.");
			}
			return false;
		}
		private static List<Tile> CreateHorizontallyFlippedTiles(Size tileSize, List<Tile> tiles)
		{
			return 
				tiles
					.Select(t => CreateHorizontallyFlippedTile(tileSize, t))
					.ToList();
		}
		private static Tile CreateHorizontallyFlippedTile(Size tileSize, Tile tile)
		{
			var polygonsFlipped =
				tile.Polygons
					.Select(p => CreateHorizontallyFlippedPolygon(tileSize, p))
					.ToList();

			var flipped = new Tile
			{
				Index = tile.Index,
				Polygons = polygonsFlipped
			};
			return flipped;
		}
		private static Polygon CreateHorizontallyFlippedPolygon(Size tileSize, Polygon polygon)
		{
			var vertexesFlipped =
				polygon
					.Vertexes
					.Select(v =>
					{
						if (tileSize.Width == 0)
							throw new ArgumentException();

						return new Point(tileSize.Width - 1 - v.X, v.Y);
					})
					.ToList();

			var flipped = new Polygon
			{
				Vertexes = vertexesFlipped,
				Purpose = polygon.Purpose,
				Tags = polygon.Tags
			};
			return flipped;
		}

		private static bool ReadInput(string input, out List<Tile> tiles)
		{
			tiles = null;
			try
			{
				tiles = ReadInput(input);
				return true;
			}
			catch
			{
				Console.WriteLine("Failed reading input.");
				return false;
			}
		}
		private static List<Tile> ReadInput(string path)
		{
			using (StreamReader file = File.OpenText(path))
			{
				JsonSerializer serializer = new JsonSerializer();
				var tiles = (List<Tile>)serializer.Deserialize(file, typeof(List<Tile>));
				return tiles;
			}
		}
		private static void WriteOutput(string path, List<Tile> tiles)
		{
			try
			{ 
				using (StreamWriter file = File.CreateText(path))
				{
					JsonSerializer serializer = new JsonSerializer();
					serializer.Serialize(file, tiles);
				}
			}
			catch
			{
				Console.WriteLine("Failed writing output.");
			}
		}
	}
}
