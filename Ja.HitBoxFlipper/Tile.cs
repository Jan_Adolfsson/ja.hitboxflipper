﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Ja.HitBoxFlip
{
	public class Tile
	{
		public Point Index { get; set; }
		public List<Polygon> Polygons { get; set; }
	}
}
